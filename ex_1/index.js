/**
 BAI TAP 1
 */

var ngayLam;
ngayLam = 12;
var luongNgayLam;
luongNgayLam = 100000;
var tongLuong = 0;
tongLuong = ngayLam * luongNgayLam;
console.log("TongLuong", tongLuong);

/**
BAI TAP 2
 */

var soThuc1 = null;
soThuc1 = 5;
var soThuc2 = null;
soThuc2 = 5;
var soThuc3 = null;
soThuc3 = 2;
var soThuc4 = null;
soThuc4 = 5;
var soThuc5 = null;
soThuc5 = 5;
var giaTriTrungBinh = null;
giaTriTrungBinh = (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5;
console.log("GiaTriTrungBinh ", giaTriTrungBinh);

/*
BAI TAP 3
*/

var soTienUSD;
soTienUSD = 2;
var menhGiaUSD;
menhGiaUSD = 23500;
var thanhTienVND;
thanhTienVND = soTienUSD * menhGiaUSD;
console.log("ThanhTienVND ", thanhTienVND);

/**
 *
 * BAI TAP 4
 */

var chieuDai;
chieuDai = 4;
var chieuRong;
chieuRong = 3;
dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong) * 2;
console.log("DienTich ", dienTich);
console.log("ChuVi", chuVi);

/**
 *
 * BAI TAP 5
 */

var nhapSo;
nhapSo = 15;
hangChuc = Math.floor(nhapSo / 10);
hangDonVi = nhapSo % 10;
var tongKySo;
tongKySo = hangChuc + hangDonVi;
console.log("TongKySo", tongKySo);
